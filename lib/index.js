'use strict';
import FileParser from "./fileparser";
import Presenter from "./presenter";

console.clear = function () {
    return process.stdout.write('\x1B[2J\x1B[0f');
};

if (process.argv.length <= 2) {
    printInstructions();
} else {
    console.clear();
    parseInput(process.argv.slice(2, process.argv.length));
}

function parseInput(args) {
    if (args.length > 1 && args[0].toString().toLowerCase() == "load") {
        try {
            merchantsGuideToTheGalaxy(args[1]);
        } catch (err) {
            console.log("uh oh, something went wrong...");
            if (/FILE NOT FOUND/.test(err.message))
                console.log(`file '${args[1]}' couldn't be found...`);
        }
    } else {
        printInstructions();
    }
}

function printInstructions() {
    console.clear();
    console.log("  ==================================");
    console.log("-== Merchant's Guide to the Galaxy ==-");
    console.log("  ==================================");
    console.log("INSTRUCTIONS:");
    console.log("- LOAD INPUT FROM FILE: `npm start -- load /path/to/file`");
    console.log("  ==================================");
}

// exported to be used by the acceptance tests
export function merchantsGuideToTheGalaxy(filePath) {
    const controller = new Presenter();
    const lines = FileParser.parse(filePath);
    for (let line of lines) {
        let out = controller.query(line);
        if (out)
            console.log(out);
    }
}