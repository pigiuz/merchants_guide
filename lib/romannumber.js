'use strict';
const symbolToValue = new Map();
symbolToValue.set("I", 1);
symbolToValue.set("V", 5);
symbolToValue.set("X", 10);
symbolToValue.set("L", 50);
symbolToValue.set("C", 100);
symbolToValue.set("D", 500);
symbolToValue.set("M", 1000);

export default class RomanNumber {

    static parse(symbols) {
        if (!symbols || symbols.replace(/ /g, '').length == 0)
            return 0;
        this.verifySymbols(symbols);

        let sum = 0;
        let prevChar = "";
        let isSubtracting = false;
        for (let i = symbols.length - 1; i >= 0; i--) {
            let char = symbols[i];
            let value = symbolToValue.get(char);
            let prevValue = prevChar == "" ? 0 : symbolToValue.get(prevChar);
            if (value <= prevValue && isSubtracting)
                throw new Error("ONLY ONE SMALL-VALUE SYMBOL CAN BE SUBTRACTED FROM ANY LARGE-VALUE SYMBOL");

            if (!this.symbolShouldToBeSubtracted(char, prevChar)) {
                isSubtracting = false;
                if (value < prevValue)
                    throw new Error("INVALID SUBTRACTION OR INVALID NUMBER");
            } else {
                value *= -1;
                isSubtracting = true;
            }
            sum += value;
            prevChar = char;
        }
        return sum;
    }

    static symbolShouldToBeSubtracted(char, prevChar) {
        return (char == "I" && (prevChar == "V" || prevChar == "X")) ||
            (char == "X" && (prevChar == "L" || prevChar == "C")) ||
            (char == "C" && (prevChar == "D" || prevChar == "M"))
    }

    static verifySymbols(symbols) {
        if (/[^MDCLXVI]/.test(symbols))
            throw new Error("NOT ROMAN SYMBOLS");

        if (symbols.indexOf("IIII") != -1
            || symbols.indexOf("XXXX") != -1
            || symbols.indexOf("CCCC") != -1) {
            throw new Error("SYMBOL CAN BE REPEATED ONLY 3 TIMES IN A ROW");
        }

        [/V/g, /D/g, /L/g].forEach(exp => {
            let matchingChars = symbols.match(exp);
            if (matchingChars && matchingChars.length > 1) {
                throw new Error(`SYMBOL ${matchingChars[0]} CANNOT BE REPEATED`);
            }
        });
    }

}