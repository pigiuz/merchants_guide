'use strict';
import should from "should";
import Presenter from "./presenter";

describe("Presenter", ()=> {
    it("on unhandled cases, it has no idea what you're talking about ", ()=> {
        let c = new Presenter();
        c.query("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?")
            .should.be.eql("I have no idea what you are talking about");
        c.query("how much is prok ?").should.be.eql("I have no idea what you are talking about");
    });

    it("simple statements (non-questions) return nothing", ()=> {
        let c = new Presenter();
        should.not.exist(c.query("glob is I"));
        should.not.exist(c.query("glob glob Silver is 10 Credits"));
    });

    it("formats output when asking 'how much is' questions", ()=> {
        let c = new Presenter();
        c.query("glob is I");
        c.query("prok is V");
        c.query("how much is glob ?").should.be.eql("glob is 1");
        c.query("how much is prok ?").should.be.eql("prok is 5");
        c.query("how much is glob prok ?").should.be.eql("glob prok is 4");
    });

    it("formats output when asking 'how many Credits' questions", ()=> {
        let c = new Presenter();
        c.query("glob is I");
        c.query("prok is V");
        c.query("glob glob Silver is 10 Credits");
        c.query("how many Credits is glob Silver ?").should.be.eql("glob Silver is 5 Credits");
        c.query("how many Credits is glob prok Silver ?").should.be.eql("glob prok Silver is 20 Credits");
    });
});