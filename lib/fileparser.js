'use strict';
import fs from "fs"

export default class FileParser {
    static parse(filePath) {
        if (!fs.existsSync(filePath)) {
            throw new Error("FILE NOT FOUND");
        } else {
            let fileContent = fs.readFileSync(filePath, "utf-8");
            return fileContent.split(/\r?\n/);
        }
    }
}