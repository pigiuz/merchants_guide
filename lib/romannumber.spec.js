'use strict';
import should from 'should';
import RomanNumber from "./romannumber";

describe("RomanNumber", ()=> {

    it("cannot parse non Roman symbols", ()=> {
        (()=> RomanNumber.parse("a")).should.throw(/NOT ROMAN SYMBOLS/);
    });

    it("can parse Roman symbols", ()=> {
        (()=> RomanNumber.parse("MDCLXVI")).should.not.throw();
    });

    it("cannot parse lower case Roman symbols", ()=> {
        (()=> RomanNumber.parse("mdclxvi")).should.throw(/NOT ROMAN SYMBOLS/);
    });

    it("returns 0 on empty input", ()=> {
        RomanNumber.parse("").should.be.equal(0);
    });

    it("returns 0 on null input", ()=> {
        RomanNumber.parse().should.be.equal(0);
    });

    it("'I','X','C' cannot be repeated more than 3 times in a row", ()=> {
        let error = /SYMBOL CAN BE REPEATED ONLY 3 TIMES IN A ROW/;
        (()=>RomanNumber.parse("IIII")).should.throw(error);
        (()=>RomanNumber.parse("XXXX")).should.throw(error);
        (()=>RomanNumber.parse("CCCC")).should.throw(error);
    });

    it("'I','X','C' can be repeated 3 times in a row", ()=> {
        (()=>RomanNumber.parse("III")).should.not.throw();
        (()=>RomanNumber.parse("XXX")).should.not.throw();
        (()=>RomanNumber.parse("CCC")).should.not.throw();
    });

    it("'V','D','L' cannot be repeated", ()=> {
        let error = /SYMBOL [VDL] CANNOT BE REPEATED/;
        (()=>RomanNumber.parse("VV")).should.throw(error);
        (()=>RomanNumber.parse("DD")).should.throw(error);
        (()=>RomanNumber.parse("LL")).should.throw(error);
    });

    it("'I' alone accounts for 1 unit", ()=> {
        RomanNumber.parse("I").should.eql(1);
    });

    it("'V' alone accounts for 5 units", ()=> {
        RomanNumber.parse("V").should.eql(5);
    });

    it("'X' alone accounts for 10 units", ()=> {
        RomanNumber.parse("X").should.eql(10);
    });

    it("'L' alone accounts for 50 units", ()=> {
        RomanNumber.parse("L").should.eql(50);
    });

    it("'C' alone accounts for 100 units", ()=> {
        RomanNumber.parse("C").should.eql(100);
    });

    it("'D' alone accounts for 500 units", ()=> {
        RomanNumber.parse("D").should.eql(500);
    });

    it("'M' alone accounts for 1000 units", ()=> {
        RomanNumber.parse("M").should.eql(1000);
    });

    it("'I' preceding V or X accounts for 1 negative unit", ()=> {
        RomanNumber.parse("IV").should.eql(4);
        RomanNumber.parse("IX").should.eql(9);
    });

    it("'X' preceding L or C accounts for negative ten", ()=> {
        RomanNumber.parse("XL").should.eql(40);
        RomanNumber.parse("XC").should.eql(90);
    });

    it("'C' preceding D or M accounts for negative hundreds", ()=> {
        RomanNumber.parse("CD").should.eql(400);
        RomanNumber.parse("CM").should.eql(900);
    });

    it("'I' can be subtracted only once", ()=> {
        let error = /ONLY ONE SMALL-VALUE SYMBOL CAN BE SUBTRACTED FROM ANY LARGE-VALUE SYMBOL/;
        (()=> RomanNumber.parse("IIV")).should.throw(error);
        (()=> RomanNumber.parse("IIX")).should.throw(error);
    });

    it("'I' can be subtracted only from 'V' or 'X'", ()=> {
        (()=> RomanNumber.parse("IV")).should.not.throw();
        (()=> RomanNumber.parse("IX")).should.not.throw();

        let error = /INVALID SUBTRACTION OR INVALID NUMBER/;
        (()=> RomanNumber.parse("IL")).should.throw(error);
        (()=> RomanNumber.parse("IC")).should.throw(error);
        (()=> RomanNumber.parse("ID")).should.throw(error);
        (()=> RomanNumber.parse("IM")).should.throw(error);
    });

    it("'X' can be subtracted only from 'L' or 'C'", ()=> {
        (()=> RomanNumber.parse("XL")).should.not.throw();
        (()=> RomanNumber.parse("XC")).should.not.throw();

        let error = /INVALID SUBTRACTION OR INVALID NUMBER/;
        (()=> RomanNumber.parse("XD")).should.throw(error);
        (()=> RomanNumber.parse("XM")).should.throw(error);
    });

    it("'C' can be subtracted only from 'D' or 'M'", ()=> {
        (()=> RomanNumber.parse("CD")).should.not.throw();
        (()=> RomanNumber.parse("CM")).should.not.throw();
    });

    it("can parse roman numbers", ()=> {
        RomanNumber.parse("MMMMMMMMMCMXCIX").should.be.eql(9999);
        RomanNumber.parse("MDCLXVI").should.be.eql(1666);
    });

});