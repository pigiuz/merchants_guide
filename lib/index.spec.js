'use strict';
import should from "should";
import {merchantsGuideToTheGalaxy} from "./index";

describe("Merchant's Guide to the Galaxy", ()=> {

    it('[ACCEPTANCE TEST] given testinput file, outputs correctly', ()=> {
        stubConsole();
        merchantsGuideToTheGalaxy("./fixtures/testinput.txt");
        restoreConsole();
        logs.should.eql([
            ["pish tegj glob glob is 42"],
            ["glob prok Silver is 68 Credits"],
            ["glob prok Gold is 57800 Credits"],
            ["glob prok Iron is 782 Credits"],
            ["I have no idea what you are talking about"]
        ]);
        logs = [];
    });
});


// utilities to replace the console with a stub
let log = console.log;
let logs = [];
let hookedLog = function () {
    logs.push(Array.prototype.slice.call(arguments));
};

function stubConsole() {
    logs = [];
    console.log = hookedLog;
}

function restoreConsole() {
    console.log = log;
}
