'use strict';
import Converter from "./converter";

export default class Presenter {
    constructor() {
        this.converter = new Converter();
    }

    query(input) {
        let out = null;
        try {
            out = this.present(this.converter.query(input));
        } catch (err) {
            out = "I have no idea what you are talking about";
        }
        return out;
    }

    present(obj) {
        if (!obj)
            return null;
        const out = obj.output instanceof Array ? obj.output.join(" ") : obj.output;
        return `${obj.input.join(" ")} is ${out}`;
    }
}