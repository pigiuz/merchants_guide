'use strict';
import RomanNumber from "./romannumber";

export default class Converter {

    constructor() {
        this.symbolsToRomans = new Map();
        this.currencyToCredits = new Map();
    }

    isHowMuchIs(tokens) {
        return tokens.length >= 4
            && tokens[0] == "how"
            && tokens[1] == "much"
            && tokens[2] == "is";
    }

    handleHowMuchIs(tokens) {
        let symbols = tokens.slice(3, tokens.length);
        if (symbols[symbols.length - 1] == "?")
            symbols.pop();

        return {
            input: symbols,
            output: this.convertToRomanNumber(symbols)
        };
    }

    convertToRomanNumber(symbols) {
        let converted = [];
        for (let i = 0; i < symbols.length; i++) {
            if (this.symbolsToRomans.has(symbols[i])) {
                converted[i] = this.symbolsToRomans.get(symbols[i]);
            } else {
                throw new Error(`CANNOT CONVERT UNDEFINED SYMBOL '${symbols[i]}'`);
            }
        }
        return RomanNumber.parse(converted.join(""));
    }

    isQuantityMapping(tokens) {
        return tokens.length === 3 && tokens[1] == "is";
    }

    handleQuantityMapping(tokens) {
        let roman = tokens[2];
        if (roman.length != 1 || /[^MDCLXVI]/.test(roman))
            throw new Error("CANNOT MAP TO INVALID ROMAN SYMBOL");
        let quantity = tokens[0];
        if (/[^a-zA-Z]/.test(quantity))
            throw new Error("QUANTITIES CAN CONTAIN LETTERS ONLY");
        this.symbolsToRomans.set(quantity, roman);
    }

    isCurrencyMapping(tokens) {
        return tokens.length >= 5
            && tokens[tokens.length - 1].toLowerCase() == "credits"
            && /[0-9]/.test(tokens[tokens.length - 2]);
    }

    handleCurrencyMapping(tokens) {
        let currency = tokens[tokens.length - 4];
        let quantity = this.convertToRomanNumber(tokens.splice(0, tokens.length - 4));
        let credits = parseInt(tokens[tokens.length - 2]);
        this.currencyToCredits.set(currency, credits / quantity);
    }

    isHowManyCredits(tokens) {
        return tokens.length >= 6
            && tokens[0].toLowerCase() == "how"
            && tokens[1].toLowerCase() == "many"
            && tokens[2].toLowerCase() == "credits"
            && tokens[3].toLowerCase() == "is"
    }

    handleHowManyCredits(tokens) {
        if (tokens[tokens.length - 1] == "?")
            tokens.pop();
        let currency = tokens[tokens.length - 1];
        const start = 4;
        const quantities = tokens.splice(start, tokens.length - start - 1);
        let quantity = this.convertToRomanNumber(quantities);
        if (!this.currencyToCredits.has(currency))
            throw new Error(`UNKNOWN CURRENCY '${currency}'`);
        return {
            input: quantities.concat(currency),
            output: [quantity * this.currencyToCredits.get(currency), "Credits"]
        };
    }

    query(q) {
        q = q.replace(/\s\s+/g, ' ');
        let tokens = q.split(" ");
        if (this.isHowMuchIs(tokens)) {
            return this.handleHowMuchIs(tokens);
        } else if (this.isHowManyCredits(tokens)) {
            return this.handleHowManyCredits(tokens);
        } else if (this.isCurrencyMapping(tokens)) {
            return this.handleCurrencyMapping(tokens);
        } else if (this.isQuantityMapping(tokens)) {
            return this.handleQuantityMapping(tokens);
        } else {
            throw new Error("UNHANDLED CASE");
        }
    }
}