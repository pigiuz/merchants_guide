'use strict';
import should from "should";
import path from "path";
import FileParser from "./fileparser";

describe("FileParser", ()=> {

    it("cannot load a non existing file", ()=> {
        (()=>FileParser.parse("/does/not/exist")).should.throw(/FILE NOT FOUND/);
    });

    it("can handle absolute paths", ()=> {
        const filePath = "./fixtures/testinput.txt";
        const absolutePath = path.resolve(filePath);
        (()=>FileParser.parse(absolutePath)).should.not.throw();
    });

    it("can handle relative paths", ()=> {
        const filePath = "./fixtures/testinput.txt";
        (()=>FileParser.parse(filePath)).should.not.throw();
    });

    it("splits output on new lines", ()=> {
        let filePath = null;
        filePath = "./fixtures/line_ending_windows.txt";
        FileParser.parse(filePath).should.be.eql(["windows", "line", "ending"]);
        filePath = "./fixtures/line_ending_unix.txt";
        FileParser.parse(filePath).should.be.eql(["unix", "line", "ending"]);
    });
});