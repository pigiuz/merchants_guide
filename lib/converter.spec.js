'use strict';
import should from "should";
import Converter from "./converter";

describe("Converter", ()=> {

    it("when no handled case is detected throws error", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("prok is V");
        converter.query("glob Silver is 1 Credits");
        (()=>converter.query("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?")).should.throw(/UNHANDLED CASE/);
    });

    it("cannot convert undefined symbols to roman numerals", ()=> {
        let converter = new Converter();
        (()=>converter.query("how much is glob ?")).should.throw("CANNOT CONVERT UNDEFINED SYMBOL 'glob'");
    });

    it("can map a quantity to a valid roman symbol", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("how much is glob ?").should.be.eql({input: ["glob"], output: 1});
    });

    it("can map multiple quantities to the same romal symbol", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("prok is I");
        converter.query("how much is glob ?").should.be.eql({input: ["glob"], output: 1});
        converter.query("how much is prok ?").should.be.eql({input: ["prok"], output: 1});
    });

    it("cannot map a quantity to an invalid roman symbol", ()=> {
        let converter = new Converter();
        (()=>converter.query("glob is b")).should.throw(/CANNOT MAP TO INVALID ROMAN SYMBOL/);
    });

    it("cannot map a quantity to a multi symbol roman number", ()=> {
        let converter = new Converter();
        (()=>converter.query("glob is IV")).should.throw(/CANNOT MAP TO INVALID ROMAN SYMBOL/);
    });

    it("quantities can contain letters only", ()=> {
        let converter = new Converter();
        (()=>converter.query("_ is I")).should.throw(/QUANTITIES CAN CONTAIN LETTERS ONLY/);
        (()=>converter.query("? is I")).should.throw(/QUANTITIES CAN CONTAIN LETTERS ONLY/);
        (()=>converter.query("1 is I")).should.throw(/QUANTITIES CAN CONTAIN LETTERS ONLY/);
        // (()=>converter.query("multi word is I")).should.throw(/QUANTITIES CAN CONTAIN LETTERS ONLY/);

        (()=>converter.query("a is I")).should.not.throw(/QUANTITIES CAN CONTAIN LETTERS ONLY/);
    });

    it("can calculate composite numbers", ()=> {
        let converter = new Converter();
        ["glob is I", "prok is V", "pish is X", "tegj is L"].forEach(input => converter.query(input))
        converter.query("how much is pish tegj glob glob ?").should.be.eql({
            input: ["pish", "tegj", "glob", "glob"],
            output: 42
        });
    });

    it("can map a currency to Credits", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("glob Silver is 1 Credits");
        converter.query("how many Credits is glob Silver ?").should.be.eql({
            input: ["glob", "Silver"],
            output: [1, "Credits"]
        });
    });

    it("can convert a composite quantity of a specific currency to Credits", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("prok is V");
        converter.query("glob Silver is 1 Credits");
        converter.query("how many Credits is glob prok Silver ?").should.be.eql({
            input: ["glob", "prok", "Silver"],
            output: [4, "Credits"]
        });
    });

    it("can convert different currencies to Credit", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        converter.query("prok is V");
        converter.query("pish is X");
        converter.query("tegj is L");
        converter.query("glob glob Silver is 34 Credits");
        converter.query("glob prok Gold is 57800 Credits");
        converter.query("pish pish Iron is 3910 Credits");
        converter.query("how many Credits is glob prok Silver ?").should.be.eql({
            input: ["glob", "prok", "Silver"],
            output: [68, "Credits"]
        });
        converter.query("how many Credits is glob prok Gold ?").should.be.eql({
            input: ["glob", "prok", "Gold"],
            output: [57800, "Credits"]
        });
        converter.query("how many Credits is glob prok Iron ?").should.be.eql({
            input: ["glob", "prok", "Iron"],
            output: [782, "Credits"]
        });
    });

    it("cannot convert to credits an unknown currency", ()=> {
        let converter = new Converter();
        converter.query("glob is I");
        (()=>converter.query("how many Credits is glob Silver ?")).should.throw(/UNKNOWN CURRENCY 'Silver'/);
    });

});